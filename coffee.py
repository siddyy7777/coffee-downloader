from time import sleep
from datetime import datetime
import sys
from csv import DictWriter
from proxy_extension import get_chromedriver, webdriver, os

startTime = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
currDir = os.path.dirname(os.path.realpath(__file__))
os.makedirs('%s/excel_data' %(currDir), exist_ok=True)
dataArray = []
column_names = []
xpathDict = {
        "search_box": "//input[@title='Search']",
        "search_button": "//input[@value='Google Search']",
        "more_places": "//div[@class='zkIadb']",
        "data_div": "//div[@class='VkpGBb']",
        "next_button": "//a[@id='pnnext']",
        "shop_name": "//div[contains(@class,'gsmt')]/span",
        "shop_website": "//div[@class='QqG1Sd']/a[contains(text(),'Website')]",
        "shop_rating": "//div[@class='Ob2kfd']/div/span[@class='rtng']",
        "shop_address": "//span[@class='LrzXr']",
        "shop_number": "//span[@data-dtype='d3ifr']/span",
        "hour_expand_button": "//span[@class='BTP3Ac']",
        "hour_table_tr": "//table[@class='WgFkxc']/tbody/tr",
    }

def findElementByXPath(driver, xpath):
    try:
        element = driver.find_element_by_xpath(xpath)
    except:
        element = None
    return element


def findElementsByXPath(driver, xpath):
    try:
        elements = driver.find_elements_by_xpath(xpath)
    except:
        elements = None
    return elements


def extractTextFromElement(driver, xpath):
    element = findElementByXPath(driver, xpath)
    if(element is None):
        return "N/A"
    else:
        return element.text


def extractAttrFromElement(driver, xpath, attr):
    element = findElementByXPath(driver, xpath)
    if(element is None):
        return "N/A"
    else:
        return element.get_attribute(attr)


def clickOnElement(driver, xPath):
    findElementByXPath(driver, xPath).click()


def extractHours(driver):
    val = ""

    try:
        clickOnElement(driver,xpathDict['hour_expand_button'])
        sleep(1)
        trList = findElementsByXPath(driver, xpathDict['hour_table_tr'])

        for row in trList:
            day = row.find_elements_by_tag_name("td")[0]
            status = row.find_elements_by_tag_name("td")[1]
            val = val + day.text + " " + status.text + "\n"
        val = val.strip()
    except:
        val = "N/A"

    return val


def writeToCsv():
    with open('%s/excel_data/data_%s.csv' %(currDir, startTime), 'w') as outfile:
        writer = DictWriter(outfile, column_names)
        writer.writeheader()
        writer.writerows(dataArray)


def iterateOverDataDivs(driver):
    elements = findElementsByXPath(driver,xpathDict['data_div'])
    length = len(elements)

    while (True):
        while(length != 0):
            #extract data
            try:
                dataDict = dict()
                elements[(length-1)].click()
                sleep(2)
                dataDict['Shop Name'] = extractTextFromElement(driver, xpathDict['shop_name'])
                dataDict['Shop Website'] = extractAttrFromElement(driver, xpathDict['shop_website'],"href")
                dataDict['Shop Address'] = extractTextFromElement(driver, xpathDict['shop_rating'])
                dataDict['Shop Rating'] = extractTextFromElement(driver, xpathDict['shop_number'])
                dataDict['Shop Number'] = extractTextFromElement(driver, xpathDict['shop_address'])
                dataDict['Shop Timing'] = extractHours(driver)
                dataArray.append(dataDict)
                column_names = list(dataDict.keys())
                print(dataDict)
            except Exception as e:
                print('Exception occured', e)
            length -= 1
        nextButton = findElementByXPath(driver, xpathDict['next_button'])

        if(nextButton is None):
            print("No more elements")
            break
        else:
            nextButton.click()
            sleep(3)
            elements = findElementsByXPath(driver, xpathDict['data_div'])
            length = len(elements)

    return column_names


def searchText(driver, keys):
    searchText = findElementByXPath(driver, xpathDict["search_box"])
    searchText.send_keys(keys)
    sleep(3)
    searchText = findElementByXPath(driver, xpathDict["search_button"])
    searchText.click()
    sleep(2)
    clickOnElement(driver, xpathDict['more_places'])


def startSearch(driver, keys):
    searchText(driver,keys)
    sleep(2)
    iterateOverDataDivs(driver)
    sleep(1)


def initializeChrome():
    chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--start-maximized')
    chrome_options.add_argument('user-data-dir=%s/selenium' %(currDir))
    #chrome_options.add_argument('--proxy-server=%s' % PROXY)
    driver = webdriver.Chrome(
                        executable_path='%s/chromedriver' %(currDir),
                        chrome_options=chrome_options,
                        service_args=['--verbose', '--log-path=%s/chromedriver.log' %(currDir)]
                    )

    return driver


def main(argv):
    # driver = initializeChrome()
    driver = get_chromedriver(use_proxy=True, path=currDir)

    try:
        keys = " ".join(argv)
        print(keys)
        keys = keys.strip()
        driver.get("https://www.google.com/")
        sleep(3)
        startSearch(driver, keys)
        sleep(2)
    finally:
        driver.close()
    
    writeToCsv()


if __name__ == '__main__':
    main(sys.argv[1:])
