from time import sleep
from datetime import datetime
import sys
from csv import DictWriter
from proxy_extension import get_chromedriver, webdriver, os

startTime = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
currDir = os.path.dirname(os.path.realpath(__file__))
os.makedirs('%s/excel_data' %(currDir), exist_ok=True)
dataArray = []
column_names = []
xpathDict = {
        "search_box": "//input[@id='query']",
        "location_box":"//input[@id='location']",
        "search_button": "//button[contains(text(),'Find')]",
        "more_places": "//div[@class='zkIadb']",
        "data_div": "//div[@class='info']",
        "next_button": "//a[@class='next ajax-page']",
        "shop_name": "//div[@class='sales-info']/h1",
        "shop_website": "//a[@class='secondary-btn website-link']",
        "shop_rating": "//div[contains(@class,'rating-stars')]",
        "shop_address": "//h2[@class='address']",
        "shop_number": "//p[@class='phone']",
        "shop_review":"//div[contains(@class,'rating-stars')]/following-sibling::span",
        "shop_email":"//a[@class='email-business']",
        "hour_table": "//dd[@class='open-hours']/div/p/time",
    }

def findElementByXPath(driver, xpath):
    try:
        element = driver.find_element_by_xpath(xpath)
    except:
        element = None
    return element


def findElementsByXPath(driver, xpath):
    try:
        elements = driver.find_elements_by_xpath(xpath)
    except:
        elements = None
    return elements


def extractTextFromElement(driver, xpath):
    element = findElementByXPath(driver, xpath)
    if(element is None):
        return "N/A"
    else:
        return element.text


def extractAttrFromElement(driver, xpath, attr):
    element = findElementByXPath(driver, xpath)
    if(element is None):
        return "N/A"
    else:
        return element.get_attribute(attr)


def clickOnElement(driver, xPath):
    findElementByXPath(driver, xPath).click()


def extractHours(driver):
    val = ""
    timings=findElementsByXPath(driver,xpathDict['hour_table'])
    if(len(timings)>0):
        for time in timings:
            val=val+time.text
    return val


def writeToCsv():
    with open('%s/excel_data/data_%s.csv' %(currDir, startTime), 'w') as outfile:
        writer = DictWriter(outfile, column_names)
        writer.writeheader()
        writer.writerows(dataArray)


def iterateOverDataDivs(driver):
    elements = findElementsByXPath(driver,xpathDict['data_div'])
    length = len(elements)
    while (True):
        while(length != 0):
            #extract data
            try:
                dataDict = dict()
                elements[(length-1)].click()
                sleep(2)
                dataDict['Name'] = extractTextFromElement(driver, xpathDict['shop_name'])
                dataDict['Website'] = extractAttrFromElement(driver, xpathDict['shop_website'],"href")
                dataDict['Address'] = extractTextFromElement(driver, xpathDict['shop_address'])
                dataDict['Rating'] = extractAttrFromElement(driver,xpathDict['shop_rating'],'class').replace("rating-stars",'') if extractAttrFromElement(driver,xpathDict['shop_rating'],'class')!='N/A' else "N/A" 
                dataDict['Number'] = extractTextFromElement(driver, xpathDict['shop_number'])
                dataDict['Email']=extractAttrFromElement(driver,xpathDict['shop_email'],'href')
                dataDict['Review']=extractTextFromElement(driver,xpathDict['shop_review'])
                dataDict['Timing'] = extractHours(driver)
                dataArray.append(dataDict)
                column_names = list(dataDict.keys())
                print(dataDict)
                driver.back()
                sleep(2)
                elements = findElementsByXPath(driver,xpathDict['data_div'])
            except Exception as e:
                print('Exception occured', e)
            length -= 1
        nextButton = findElementByXPath(driver, xpathDict['next_button'])
        if(nextButton is None):
            print("No more elements")
            break
        else:
            nextButton.click()
            sleep(3)
            elements = findElementsByXPath(driver, xpathDict['data_div'])
            length = len(elements)

    return column_names


def searchText(driver, keys):
    k=keys.split(',')
    searchUrl="https://www.yellowpages.com/search?search_terms=%s&geo_location_terms=%s" %(k[0],k[1])
    print(searchUrl) 
    driver.get(searchUrl)
    sleep(4)
    


def startSearch(driver, keys):
    print("starting search")
    searchText(driver,keys)
    sleep(2)
    iterateOverDataDivs(driver)
    sleep(1)


def initializeChrome():
    chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--start-maximized')
    #chrome_options.add_argument('user-data-dir=%s/selenium' %(currDir))
    #chrome_options.add_argument('--proxy-server=%s' % PROXY)
    driver = webdriver.Chrome(
                        executable_path='%s/chromedriver' %(currDir),
                        chrome_options=chrome_options
                    )

    return driver


def main(argv):
    driver = initializeChrome()
    #driver = get_chromedriver(use_proxy=True, path=currDir)
    try:
        keys = " ".join(argv)
        print(keys)
        keys = keys.strip()
        startSearch(driver, keys)
        sleep(2)
    finally:
        driver.close()
    writeToCsv()


if __name__ == '__main__':
    main(sys.argv[1:])
